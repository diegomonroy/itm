<?php

/* ************************************************************************************************************************

ITM

File:			payu_confirmation.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2019

************************************************************************************************************************ */

// Variables
switch ( $_POST['state_pol'] ) {
	case 4:
		$state_pol_text = 'Transacción aprobada';
		break;
	case 6:
		$state_pol_text = 'Transacción rechazada';
		break;
	case 5:
		$state_pol_text = 'Transacción expirada';
		break;
}
$reference_sale = $_POST['reference_sale'];
$reference_pol = $_POST['reference_pol'];
$value = '$' . number_format( $_POST['value'] );
$currency = $_POST['currency'];
$email_buyer = $_POST['email_buyer'];
// Email
$to = 'comercial2@itmcol.com';
$subject = 'ITM';
$message = '<html>';
$message .= '<head>';
$message .= '<title>ITM</title>';
$message .= '</head>';
$message .= '<body>';
$message .= '<table>';
$message .= '<tr><td>Estado de Transacción: </td><td>' . $state_pol_text . '</td></tr>';
$message .= '<tr><td>Referencia de Transacción: </td><td>' . $reference_sale . '</td></tr>';
$message .= '<tr><td>Referencia de Venta: </td><td>' . $reference_pol . '</td></tr>';
$message .= '<tr><td>Cantidad Total: </td><td>' . $value . '</td></tr>';
$message .= '<tr><td>Moneda: </td><td>' . $currency . '</td></tr>';
$message .= '<tr><td>Email: </td><td>' . $email_buyer . '</td></tr>';
$message .= '</table>';
$message .= '</body>';
$message .= '</html>';
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'To: ITM <comercial2@itmcol.com>' . "\r\n";
$headers .= 'From: ITM <comercial2@itmcol.com>' . "\r\n";
$headers .= 'Cc: <' . $email_buyer . '>' . "\r\n";
mail( $to, $subject, $message, $headers );

?>
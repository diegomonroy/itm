		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php if ( is_page( array( 'inicio-agroindustrial', 'inicio-infraestructura-e-hidrocarburos' ) ) ) : get_template_part( 'part', 'block-3' ); endif; ?>
		<?php if ( is_page( array( 'inicio-infraestructura-e-hidrocarburos' ) ) ) : get_template_part( 'part', 'block-6' ); endif; ?>
		<?php if ( is_page( array( 'inicio-agroindustrial') ) ) : get_template_part( 'part', 'block-4' ); endif; ?>
		<?php if ( is_page( array( 'inicio-infraestructura-e-hidrocarburos' ) ) ) : get_template_part( 'part', 'block-7' ); endif; ?>
		<?php if ( is_front_page() || is_page( array( 'contactenos' ) ) ) {} else { get_template_part( 'part', 'block-5' ); } ?>
		<?php get_template_part( 'part', 'block-2' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php if ( ! is_front_page() ) : dynamic_sidebar( 'pago' ); endif; ?>
		<?php get_template_part( 'part', 'jivosite' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>
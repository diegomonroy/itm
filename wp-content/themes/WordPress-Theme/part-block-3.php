<!-- Begin Block 3 -->
	<section class="block_3" data-wow-delay="0.5s">
		<div class="row block_title">
			<div class="small-12 columns">
				<h3 class="text-center">ÚLTIMAS NOTICIAS</h3>
			</div>
		</div>
		<div class="row collapse expanded block_content">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_3' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 3 -->
<!-- Begin Top -->
	<section class="top green" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns text-center show-for-small-only">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 columns text-center">
				<?php dynamic_sidebar( 'menu_agroindustrial' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->
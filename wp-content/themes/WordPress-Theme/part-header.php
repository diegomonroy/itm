<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<?php if ( is_front_page() ) : ?>
			<div class="small-12 columns text-center">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<?php else : ?>
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<?php endif; ?>
		</div>
	</section>
<!-- End Top -->
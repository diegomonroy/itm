<!DOCTYPE html>
<html class="no-js" lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php get_template_part( 'part', 'google-webmaster-tools' ); ?>
		<title><?php bloginfo(title); ?></title>
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo site_url(); ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php bloginfo(name); ?>">
		<meta property="og:description" content="<?php bloginfo(description); ?>">
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/build/logo_ogp.png">
		<link rel="image_src" href="<?php echo get_template_directory_uri(); ?>/build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/build/favicon.ico">
		<!-- Begin Google hreflang -->
		<?php get_template_part( 'part', 'google-hreflang' ); ?>
		<!-- End Google hreflang -->
		<!-- Begin Google Analytics -->
		<?php get_template_part( 'part', 'google-analytics' ); ?>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php get_template_part( 'part', 'header' ); ?>
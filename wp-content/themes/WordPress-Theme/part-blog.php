<!-- Begin Content -->
	<section class="content green special" data-wow-delay="0.5s">
		<div class="row" data-equalizer data-equalize-on="medium" id="equalizer">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="small-12 medium-4 columns category_main">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="row align-center align-middle">
							<div class="small-12 columns">
								<p class="text-center"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></p>
							</div>
							<div class="small-12 columns" data-equalizer-watch>
								<h2 class="text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php the_excerpt(); ?>
							</div>
							<div class="small-12 columns">
								<p class="text-center"><a href="<?php the_permalink(); ?>" class="button">Leer más...</a></p>
							</div>
						</div>
					</article>
				</div>
			<?php endwhile; endif; ?>
		</div>
	</section>
<!-- End Content -->